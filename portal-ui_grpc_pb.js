// GENERATED CODE -- DO NOT EDIT!

'use strict';
var grpc = require('grpc');
var portal$ui_pb = require('./portal-ui_pb.js');
var google_protobuf_timestamp_pb = require('google-protobuf/google/protobuf/timestamp_pb.js');

function serialize_ui_AuthReq(arg) {
  if (!(arg instanceof portal$ui_pb.AuthReq)) {
    throw new Error('Expected argument of type ui.AuthReq');
  }
  return new Buffer(arg.serializeBinary());
}

function deserialize_ui_AuthReq(buffer_arg) {
  return portal$ui_pb.AuthReq.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_ui_AuthResp(arg) {
  if (!(arg instanceof portal$ui_pb.AuthResp)) {
    throw new Error('Expected argument of type ui.AuthResp');
  }
  return new Buffer(arg.serializeBinary());
}

function deserialize_ui_AuthResp(buffer_arg) {
  return portal$ui_pb.AuthResp.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_ui_GetNodeStatusReq(arg) {
  if (!(arg instanceof portal$ui_pb.GetNodeStatusReq)) {
    throw new Error('Expected argument of type ui.GetNodeStatusReq');
  }
  return new Buffer(arg.serializeBinary());
}

function deserialize_ui_GetNodeStatusReq(buffer_arg) {
  return portal$ui_pb.GetNodeStatusReq.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_ui_GetNodeStatusResp(arg) {
  if (!(arg instanceof portal$ui_pb.GetNodeStatusResp)) {
    throw new Error('Expected argument of type ui.GetNodeStatusResp');
  }
  return new Buffer(arg.serializeBinary());
}

function deserialize_ui_GetNodeStatusResp(buffer_arg) {
  return portal$ui_pb.GetNodeStatusResp.deserializeBinary(new Uint8Array(buffer_arg));
}


var UiService = exports.UiService = {
  getNodeStatus: {
    path: '/ui.Ui/GetNodeStatus',
    requestStream: false,
    responseStream: false,
    requestType: portal$ui_pb.GetNodeStatusReq,
    responseType: portal$ui_pb.GetNodeStatusResp,
    requestSerialize: serialize_ui_GetNodeStatusReq,
    requestDeserialize: deserialize_ui_GetNodeStatusReq,
    responseSerialize: serialize_ui_GetNodeStatusResp,
    responseDeserialize: deserialize_ui_GetNodeStatusResp,
  },
  auth: {
    path: '/ui.Ui/Auth',
    requestStream: false,
    responseStream: false,
    requestType: portal$ui_pb.AuthReq,
    responseType: portal$ui_pb.AuthResp,
    requestSerialize: serialize_ui_AuthReq,
    requestDeserialize: deserialize_ui_AuthReq,
    responseSerialize: serialize_ui_AuthResp,
    responseDeserialize: deserialize_ui_AuthResp,
  },
};

exports.UiClient = grpc.makeGenericClientConstructor(UiService);
